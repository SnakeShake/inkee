/*
 *  ARSimpleApplication.java
 *  ARToolKit5
 *
 *  Disclaimer: IMPORTANT:  This Daqri software is supplied to you by Daqri
 *  LLC ("Daqri") in consideration of your agreement to the following
 *  terms, and your use, installation, modification or redistribution of
 *  this Daqri software constitutes acceptance of these terms.  If you do
 *  not agree with these terms, please do not use, install, modify or
 *  redistribute this Daqri software.
 *
 *  In consideration of your agreement to abide by the following terms, and
 *  subject to these terms, Daqri grants you a personal, non-exclusive
 *  license, under Daqri's copyrights in this original Daqri software (the
 *  "Daqri Software"), to use, reproduce, modify and redistribute the Daqri
 *  Software, with or without modifications, in source and/or binary forms;
 *  provided that if you redistribute the Daqri Software in its entirety and
 *  without modifications, you must retain this notice and the following
 *  text and disclaimers in all such redistributions of the Daqri Software.
 *  Neither the name, trademarks, service marks or logos of Daqri LLC may
 *  be used to endorse or promote products derived from the Daqri Software
 *  without specific prior written permission from Daqri.  Except as
 *  expressly stated in this notice, no other rights or licenses, express or
 *  implied, are granted by Daqri herein, including but not limited to any
 *  patent rights that may be infringed by your derivative works or by other
 *  works in which the Daqri Software may be incorporated.
 *
 *  The Daqri Software is provided by Daqri on an "AS IS" basis.  DAQRI
 *  MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 *  THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE, REGARDING THE DAQRI SOFTWARE OR ITS USE AND
 *  OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 *
 *  IN NO EVENT SHALL DAQRI BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 *  OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 *  MODIFICATION AND/OR DISTRIBUTION OF THE DAQRI SOFTWARE, HOWEVER CAUSED
 *  AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 *  STRICT LIABILITY OR OTHERWISE, EVEN IF DAQRI HAS BEEN ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *  Copyright 2015 Daqri, LLC.
 *  Copyright 2011-2015 ARToolworks, Inc.
 *
 *  Author(s): Philip Lamb
 *
 */

//
// This class provides a subclass of Application to enable app-wide behavior.
// 

package it.inkee.ar.application;

import android.app.Application;
import android.util.Log;

import org.artoolkit.ar.base.assets.AssetHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import it.inkee.ar.marker.LoadNavigationMarker;
import it.inkee.ar.marker.LoadOtherMarker;
import it.inkee.ar.marker.other.OtherMarker;
import it.inkee.ar.marker.path.NavigationMarker;
import it.inkee.ar.renderer.LoadModel;
import it.inkee.ar.utils.InkeeSpeechToText;
import it.inkee.ar.utils.InkeeTextToSpeech;
import nz.gen.geek_central.GLUseful.ObjReader.Model;

public class InkeeApplication extends Application {

	public final static int MODE_DEMO = 0;
	public final static int MODE_LIVE = 1;

	private static final String TAG = InkeeApplication.class
			.getSimpleName();

	private static Application sInstance;

	private InkeeSpeechToText stt;
	private InkeeTextToSpeech tts;

	private List<OtherMarker> oMarkers;
	private List<NavigationMarker> nMarkers;

	private int currMode = MODE_LIVE;

	// Anywhere in the application where an instance is required, this method
	// can be used to retrieve it.
	public static Application getInstance() {
		return sInstance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		sInstance = this;

		((InkeeApplication) sInstance).initializeInstance();
	}

	// Here we do one-off initialisation which should apply to all activities
	// in the application.
	protected void initializeInstance() {

		// Unpack assets to cache directory so native library can read them.
		// N.B.: If contents of assets folder changes, be sure to increment the
		// versionCode integer in the AndroidManifest.xml file.
		AssetHelper assetHelper = new AssetHelper(getAssets());
		assetHelper.cacheAssetFolder(getInstance(), "Data");

		if (stt == null)
			stt = new InkeeSpeechToText(this);
		if (tts == null)
			tts = new InkeeTextToSpeech(this);
	}

	public void setDemoMode() {
	    currMode = MODE_DEMO;
    }

    public boolean isInDemoMode() {
	    return currMode == MODE_DEMO;
    }

	public List<OtherMarker> getOtherMarkers() {

		if (oMarkers == null) {
			// load allMarkers
			LoadOtherMarker allMarkers = new LoadOtherMarker();
			allMarkers.load(getApplicationContext());
			oMarkers = allMarkers.getOtherMarkers();
		}

		return oMarkers;

	}

	public List<NavigationMarker> getNavigationMarkers() {

		HashMap<String, Model> mapModels;

		if (nMarkers == null) {
			// time consuming. load everything and then use it
			LoadModel loadModels = new LoadModel();
			loadModels.load(getAssets(), "models");
			mapModels = loadModels.getModels();
			
			for(String name : mapModels.keySet()){
				Log.i(TAG, "model with name: " + name + "loaded");
			}
			

			// load the itinerary
			LoadNavigationMarker itinerary = new LoadNavigationMarker();
			itinerary.loadItinerary(getApplicationContext());
			// need flexibility to draw stuff
			itinerary.setModelToDraw(mapModels);

			nMarkers = itinerary.getItinerary();

		}

		return nMarkers;

	}

}
