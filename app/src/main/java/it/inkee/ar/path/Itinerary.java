package it.inkee.ar.path;

import android.content.Context;
import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import it.inkee.ar.utils.Rotation;
import it.inkee.ar.utils.Scale;
import it.inkee.ar.utils.Transformation;
import it.inkee.ar.utils.Translate;
import nz.gen.geek_central.GLUseful.ObjReader.Model;

/**
 * Utility class used to retrieve the itinerary that the user has to follow to
 * reach the product.
 * 
 * @author mutti
 * 
 */
public class Itinerary {

	private List<Marker> path;

	public Itinerary() {

	}

	/**
	 * 
	 * This method allows to retrieve the itinerary from a json file.
	 * 
	 * @param context
	 *            Application context
	 * @return true
	 */
	public boolean loadItinerary(Context context) {

		InputStream is;
		try {
			is = context.getAssets().open("path/demo.json");

			path = readItinerary(is);

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * This method returns the itinerary as a list of markers.
	 * 
	 * @return a list of markers representing the itinerary
	 */
	public List<Marker> getItinerary() {
		return path;
	}

	public List<Marker> readItinerary(InputStream in) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		try {
			return readItineraryInternal(reader);
		} finally {
			reader.close();
		}
	}

	public List<Marker> readItineraryInternal(JsonReader reader)
			throws IOException {
		List<Marker> itinerary = new ArrayList<Marker>();

		reader.beginArray();
		while (reader.hasNext()) {
			itinerary.add(readMarker(reader));
		}
		reader.endArray();
		return itinerary;
	}

	/**
	 * this method allows to parse the json file and retrieve the information
	 * about a marker.
	 * 
	 * @param reader
	 * @return a new marker of the itinerary
	 * @throws IOException
	 */
	public Marker readMarker(JsonReader reader) throws IOException {
		String type = null;
		boolean defaultz = false;
		boolean isEnd = false;
		Scale scale = null;
		boolean needOrientation = false;
		String turn = null;
		float[] transformationMatrix = null;
		List<Transformation> nearTrans = null;
		List<Transformation> farTrans = null;
		String nearImageName = null;
		String farImageName = null;

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("default")) {
				defaultz = reader.nextBoolean();
			} else if (name.equals("type")) {
				type = reader.nextString();
			} else if (name.equals("scale") && reader.peek() != JsonToken.NULL) {
				List<Float> list = readDoublesArray(reader);
				scale = new Scale(list.get(0), list.get(1), list.get(2));
			} else if (name.equals("far")) {
				farTrans = readTransformation(reader);
			} else if (name.equals("near")) {
				nearTrans = readTransformation(reader);
			} else if (name.equals("transformationMatrix")
					&& reader.peek() != JsonToken.NULL) {
				List<Float> list = readDoublesArray(reader);

				transformationMatrix = new float[list.size()];
				int i = 0;
				for (Float f : list) {
					transformationMatrix[i++] = (f != null ? f : Float.NaN);
				}

			} else if (name.equals("turn")) {
				turn = reader.nextString();
			} else if (name.equals("orientation")) {
				needOrientation = reader.nextBoolean();
			} else if (name.equals("nearImageName")) {
				nearImageName = reader.nextString();
			} else if (name.equals("farImageName")) {
				farImageName = reader.nextString();
			} else if (name.equals("isEnd")) {
				isEnd = reader.nextBoolean();
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();

		return new Marker(type, defaultz, scale, transformationMatrix,
				needOrientation, turn, nearTrans, farTrans, nearImageName,
				farImageName, isEnd);
	}

	public List<Float> readDoublesArray(JsonReader reader) throws IOException {
		List<Float> matrix = new ArrayList<Float>();

		reader.beginArray();
		while (reader.hasNext()) {
			matrix.add((float) reader.nextDouble());
		}
		reader.endArray();
		return matrix;
	}

	/**
	 * 
	 * Read the transformation to apply to the object.
	 * 
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	public List<Transformation> readTransformation(JsonReader reader)
			throws IOException {
		List<Float> list = new ArrayList<Float>();
		List<Transformation> rotations = new ArrayList<Transformation>();

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			// is a number?
			if (name.matches("rotate")) {
				list = readDoublesArray(reader);
				Rotation r = new Rotation(list.get(0), list.get(1),
						list.get(2), list.get(3));
				rotations.add(r);
			} else if (name.matches("translate")) {
				list = readDoublesArray(reader);
				Translate t = new Translate(list.get(0), list.get(1),
						list.get(2));
				rotations.add(t);
			} else {
				reader.skipValue();
			}

		}
		reader.endObject();

		return rotations;
	}

	public void setModelToDraw(HashMap<String, Model> mapModels) {

		List<Marker> it = getItinerary();
		for (Marker m : it) {
			if (m.getNearImageName() != null) {
				m.setNearModel(mapModels.get(m.getNearImageName()));
			}

			if (m.getFarImageName() != null) {
				m.setFarModel(mapModels.get(m.getFarImageName()));
			}

		}
	}

}
