package it.inkee.ar.sensor.provider;//package it.inkee.sensor.provider;
//
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//
///**
// * Moverio SDK does not have a step counter, API too old.
// * 
// * @author mutti
// * 
// */
//public class StepProvider implements SensorEventListener {
//
//	private SensorManager mSensorManager;
//
//	private Sensor mStepCounterSensor;
//
//	private Sensor mStepDetectorSensor;
//
//	public StepProvider(SensorManager sensorManager) {
//
//		this.mSensorManager = sensorManager;
//
//		mStepCounterSensor = mSensorManager
//				.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
//		mStepDetectorSensor = mSensorManager
//				.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
//
//	}
//
//	@Override
//	public void onAccuracyChanged(Sensor sensor, int accuracy) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void onSensorChanged(SensorEvent event) {
//		// TODO Auto-generated method stub
//
//	}
//
//	/**
//	 * Starts the sensor fusion (e.g. when resuming the activity)
//	 */
//	public void start() {
//
//		mSensorManager.registerListener(this, mStepCounterSensor,
//				SensorManager.SENSOR_DELAY_GAME);
//
//		mSensorManager.registerListener(this, mStepDetectorSensor,
//				SensorManager.SENSOR_DELAY_GAME);
//
//	}
//
//	/**
//	 * Stops the sensor fusion (e.g. when pausing/suspending the activity)
//	 */
//	public void stop() {
//
//		mSensorManager.unregisterListener(this, mStepCounterSensor);
//		mSensorManager.unregisterListener(this, mStepDetectorSensor);
//
//	}
//
//}
