package it.inkee.ar.marker;

import android.content.Context;
import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import it.inkee.ar.marker.other.OtherMarker;

/**
 * Utility class used to retrieve the list of markers not involved in the navigation phase.
 * 
 * @author mutti
 * 
 */
public class LoadOtherMarker {

	private List<OtherMarker> markers;

	public LoadOtherMarker() {

	}

	/**
	 * 
	 * This method allows to retrieve the list of markers not involved in the navigation phase.
	 * 
	 * @param context
	 *            Application context
	 * @return true
	 */
	public boolean load(Context context) {

		InputStream is;
		try {
			is = context.getAssets().open("other/demo.json");

			markers = readItinerary(is);

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * This method returns the itinerary as a list of markers.
	 * 
	 * @return a list of markers representing the itinerary
	 */
	public List<OtherMarker> getOtherMarkers() {
		return markers;
	}

	public List<OtherMarker> readItinerary(InputStream in) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		try {
			return readOtherMarkerInternal(reader);
		} finally {
			reader.close();
		}
	}

	public List<OtherMarker> readOtherMarkerInternal(JsonReader reader)
			throws IOException {
		List<OtherMarker> m = new ArrayList<OtherMarker>();

		reader.beginArray();
		while (reader.hasNext()) {
			m.add(readMarker(reader));
		}
		reader.endArray();
		return m;
	}

	/**
	 * this method allows to parse the json file and retrieve the information
	 * about a marker that is not involved in the navigation phase.
	 * 
	 * @param reader
	 * @return a new marker of the itinerary
	 * @throws IOException
	 */
	public OtherMarker readMarker(JsonReader reader) throws IOException {
		String type = null;
		String phase = null;
		String extra = null;

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("type")) {
				type = reader.nextString();
			} else if (name.equals("phase")) {
				phase = reader.nextString();
			} else if (name.equals("extra")) {
				extra = reader.nextString();
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();

		return new OtherMarker(type, phase, extra);
	}

}
