package it.inkee.ar.marker.path;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import it.inkee.ar.utils.Rotation;
import it.inkee.ar.utils.Scale;
import it.inkee.ar.utils.Transformation;
import it.inkee.ar.utils.Translate;
import nz.gen.geek_central.GLUseful.ObjReader.Model;

/**
 * 
 * Utility class to represent an ARMarker.
 * 
 * @author mutti
 * 
 */
public class NavigationMarker {

	private int markerID = -1;

	private String type = "";

	private Scale scale = null;

	private float[] transformationMatrix = null;

	private boolean showObject = false;

	/**
	 * This attribute could assume the following values RIGTH, LEFT, TOP, DOWN
	 */
	private String turn = "";

	private List<Transformation> nearTransformation = null;
	private List<Transformation> farTransformation = null;

	private boolean isNear = false;

	private float cameraToMarkerDistance;

	private List<Transformation> additionalTransformations = new ArrayList<Transformation>();

	private boolean needOrientation = false;

	private boolean isDefaultMarker = false;

	private String nearImageName = null;
	private String farImageName = null;

	private Model nearModel = null;
	private Model farModel = null;

	private boolean isEnd = false;

	private boolean farClockwise = false;
	private boolean nearClockwise = false;

	public NavigationMarker() {

	}

	public NavigationMarker(String type, boolean defaultz, Scale scale,
			float[] matrix, boolean needOrientation, String turn,
			List<Transformation> nearTransformation,
			List<Transformation> farTransformation, String nearImageName,
			String farImageName, boolean isEnd, boolean nearClockwise,
			boolean farClockwise) {

		this.markerID = getIDfromType(type);
		this.type = type;
		this.setDefaultMarker(defaultz);
		this.scale = scale;
		this.transformationMatrix = matrix;
		this.needOrientation = needOrientation;
		this.turn = turn;
		this.nearTransformation = nearTransformation;
		this.farTransformation = farTransformation;

		this.nearImageName = nearImageName;
		this.farImageName = farImageName;

		this.setEnd(isEnd);

		this.nearClockwise = nearClockwise;
		this.farClockwise = farClockwise;

		showObject = false;
		isNear = false;
		additionalTransformations.clear();

		if (this.farTransformation == null)
			this.isNear = true;

	}

	private int getIDfromType(String markerType) {
		String[] str = markerType.split(";");

		try {

			if (str[0].equals("single_barcode")) {
				int id = Integer.parseInt(str[1]);
				return id;
			}

		} catch (Exception e) {
			return -1;
		}

		return -1;
	}

	public int getMarkerID() {
		return markerID;
	}

	public void setMarkerID(int markerID) {
		this.markerID = markerID;
	}

	public float[] getTransformationMatrix() {
		return transformationMatrix;
	}

	public void setTransformationMatrix(float[] transformationMatrix) {
		this.transformationMatrix = transformationMatrix;
	}

	public boolean isShowObject() {
		return showObject;
	}

	public void setShowObject(boolean showObject) {
		this.showObject = showObject;
	}

	public String getWhereToTurn() {
		return turn;
	}

	public void setWhereToTurn(String whereToTurn) {
		this.turn = whereToTurn;
	}

	public List<Transformation> getNearTransformation() {
		return nearTransformation;
	}

	public void setNearTransformation(List<Transformation> trans) {
		this.nearTransformation = trans;
	}

	public boolean isNear() {
		if (isEnd() || farTransformation != null)
			return isNear;

		return true;
	}

	public void setNear(boolean isNear) {
		if (isEnd() || farTransformation != null)
			this.isNear = isNear;
	}

	public float getCameraToMarkerDistance() {
		return cameraToMarkerDistance;
	}

	public void setCameraToMarkerDistance(float cameraToMarkerDistance) {
		this.cameraToMarkerDistance = cameraToMarkerDistance;
	}

	public List<Transformation> getAdditionalTransformations() {
		return additionalTransformations;
	}

	public void addAdditionalTransformation(Transformation trans) {
		synchronized (additionalTransformations) {
			this.additionalTransformations.add(trans);
		}
	}

	public void setAdditionalTransformations(List<Transformation> trans) {
		synchronized (additionalTransformations) {
			this.additionalTransformations = trans;
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Scale getScale() {
		return scale;
	}

	public void setScale(Scale scale) {
		this.scale = scale;
	}

	public List<Transformation> getFarTransformation() {
		return farTransformation;
	}

	public void setFarTransformation(List<Transformation> trans) {
		this.farTransformation = trans;
	}

	public List<Transformation> getTransformations() {

		// default arrow
		if (isDefaultMarker())
			return getNearTransformation();

		// up, down
		if (getFarTransformation() == null)
			return getNearTransformation();

		// left, right
		if (isNear()) {
			return getNearTransformation();
		} else
			return getFarTransformation();

	}

	public boolean needOrientation() {
		return this.needOrientation;
	}

	public void setNeedOrientation(boolean orientation) {
		this.needOrientation = orientation;
	}

	public void clearAdditionalTransformations() {
		synchronized (additionalTransformations) {
			this.additionalTransformations.clear();
		}

	}

	public boolean isDefaultMarker() {
		return isDefaultMarker;
	}

	public void setDefaultMarker(boolean isDefaultMarker) {
		this.isDefaultMarker = isDefaultMarker;
	}

	/**
	 * 
	 * @param gl
	 * @param list
	 */
	public void applyTransformation(GL10 gl, List<Transformation> list) {

		gl.glPushMatrix();

		// need to be thread safe
		synchronized (list) {
			Iterator<Transformation> i = list.iterator();
			while (i.hasNext()) {
				Transformation t = i.next();
				if (t instanceof Rotation) {
					Rotation rotation = (Rotation) t;
					gl.glRotatef(rotation.getAngle(), rotation.getX(),
							rotation.getY(), rotation.getZ());
				} else if (t instanceof Translate) {
					Translate translate = (Translate) t;
					gl.glTranslatef(translate.getX(), translate.getY(),
							translate.getZ());
				}
			}
		}
	}

	public String getNearImageName() {
		return nearImageName;
	}

	public void setNearImageName(String nearImage) {
		this.nearImageName = nearImage;
	}

	public String getFarImageName() {
		return farImageName;
	}

	public void setFarImageName(String farImage) {
		this.farImageName = farImage;
	}

	public boolean isEnd() {
		return isEnd;
	}

	public void setEnd(boolean isEnd) {
		this.isEnd = isEnd;
	}

	public Model getNearModel() {
		return nearModel;
	}

	public void setNearModel(Model nearModel) {
		this.nearModel = nearModel;
	}

	public Model getFarModel() {
		if (isEnd())
			return nearModel;

		return farModel;
	}

	public void setFarModel(Model farModel) {
		this.farModel = farModel;
	}

	public boolean isFarClockwise() {
		return farClockwise;
	}

	public void setFarClockwise(boolean farClockwise) {
		this.farClockwise = farClockwise;
	}

	public boolean isNearClockwise() {
		return nearClockwise;
	}

	public void setNearClockwise(boolean nearClockwise) {
		this.nearClockwise = nearClockwise;
	}
}
