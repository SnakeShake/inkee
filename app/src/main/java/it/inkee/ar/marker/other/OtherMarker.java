package it.inkee.ar.marker.other;

import it.inkee.ar.utils.InkeeUtils;

/**
 * 
 * Utility class to represent an ARMarker.
 * 
 * @author mutti
 * 
 */
public class OtherMarker {

	private int markerID = -1;

	private String type = "";

	private String phase = null;
	private String extra = null;

	public OtherMarker() {

	}

	public OtherMarker(String type, String phase, String extra) {

		this.markerID = InkeeUtils.getIDfromType(type);
		this.type = type;
		this.phase = phase;
		this.extra = extra;

	}

	public int getMarkerID() {
		return markerID;
	}

	public void setMarkerID(int markerID) {
		this.markerID = markerID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
}
