package it.inkee.ar.utils;

/**
 * Rotation is a wrapper for an openGL rotation.
 * 
 * @author mutti
 * 
 */
public class Rotation extends Transformation {

	private float x;
	private float y;
	private float z;

	private float angle;

	public Rotation(float angle, float x, float y, float z) {
		super();
		this.setAngle(angle);
		this.setX(x);
		this.setY(y);
		this.setZ(z);
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float getAngle() {
		return angle;
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}

}
