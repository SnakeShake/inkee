package it.inkee.ar.utils;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.List;

import it.inkee.ar.marker.path.NavigationMarker;
import it.inkee.ar.sensor.representation.Quaternion;
import jp.epson.moverio.bt200.DisplayControl;
import jp.epson.moverio.bt200.SensorControl;

/**
 * Utility class used to store shared data.
 * 
 * @author mutti
 * 
 */
public class InkeeUtils {

	private static final String TAG = InkeeUtils.class.getSimpleName();

	public static int TTS_CHECK_CODE = 100;

	public static final int SCANNER_REQUEST_CODE = 200;

	public static final int ORDERS_LIST_CODE = 300;
	public static final int ORDER_ACCEPTED = 400;

	public static final String LOGIN_PHASE = "login";
	public static final String SCAN_PHASE = "scan";

	public static String INKEE_CONFIRMED = "confermato";
	public static String INKEE_SCAN = "scan";
	public static String INKEE_ARTICLE = "articolo";
	public static String INKEE_ORDERS_LIST = "ordini";

	/**
	 * super secure :)
	 * 
	 * authenticated user
	 */
	public static String[] authUsers = { "simone mutti", "tester", "roberto ravetta", "mario arrigoni neri" };

	public static String INKEE_ACTION_ORDER = "start-order-activity";
	public static String INKEE_ACTION_ORDER_CONFIRMED = "order-confirmed";
	public static String INKEE_ACTION_SCAN = "start-scan-activity";
	public static String INKEE_ACTION_UPDATE_DISTANCE = "update-distance";
	public static String INKEE_ACTION_LOGIN = "inkee-login";
	public static String INKEE_ACTION_ITEM_SCANNED = "inkee-item-scanned";

	public static String INKEE_ACTION_SHOW_INFO = "inkee-show-info";

	public static String INKEE_ACTION_SPEAK = "inkee-speak";

	public static int DELAY = 30;

	public static String activity;

	public static boolean isRendererInit = false;

	public static void setActivityVisible(String activity) {
		InkeeUtils.activity = activity;
	}

	public static void setRendererInit(boolean init) {
		InkeeUtils.isRendererInit = init;
	}

	/**
	 * This method allows to setup the stage for the moverio. Basically, put the
	 * app in fullscreen mode (hide system bar).
	 * 
	 * @param currentActivity
	 * @param sensorControl
	 * @param sensorMode
	 * @param display
	 * @param window
	 */
	public static void moverioScreenCustomization(Activity currentActivity,
			int sensorMode, Window window, int backlight, boolean mode3D) {

		try {

			DisplayControl display;

			// check whether the app is executed on an EPSON device or not
			if (Build.MANUFACTURER.equalsIgnoreCase("EPSON")) {
				new SensorControl(currentActivity).setMode(sensorMode);
				display = new DisplayControl(currentActivity);
				display.setBacklight(backlight);

				if (mode3D)
					display.setMode(DisplayControl.DISPLAY_MODE_3D, false);
				else
					display.setMode(DisplayControl.DISPLAY_MODE_2D, false);

			} else
				Log.i(TAG, "The app is not executed on a Moverio");

		} catch (Exception e) {
			Log.e(TAG, "jp.epson.moverio.bt200.DisplayControl is missing");
		} finally {

			// Remove title bar
			currentActivity.requestWindowFeature(Window.FEATURE_NO_TITLE);

			// Remove bottom bar
			WindowManager.LayoutParams winParams = window.getAttributes();
			winParams.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
			winParams.flags |= 0x80000000;
			window.setAttributes(winParams);
		}
	}

	/**
	 * 
	 * Copied by
	 * http://www.euclideanspace.com/maths/geometry/rotations/conversions
	 * /quaternionToEuler/
	 * 
	 * this method allows to get the heading in degree from a quaternion.
	 * 
	 * @param quaternion
	 *            representing the rotation matrix
	 * @return
	 */
	public static double getHeading(Quaternion quaternion) {
		double heading;
		double attitude;
		double bank;

		double sqw = quaternion.getW() * quaternion.getW();
		double sqx = quaternion.getX() * quaternion.getX();
		double sqy = quaternion.getY() * quaternion.getY();
		double sqz = quaternion.getZ() * quaternion.getZ();
		double unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise
												// is correction factor
		double test = quaternion.getX() * quaternion.getY() + quaternion.getZ()
				* quaternion.getW();
		if (test > 0.499 * unit) { // singularity at north pole
			heading = 2 * Math.atan2(quaternion.getX(), quaternion.getX());
			attitude = Math.PI / 2;
			bank = 0;
			return 0;
		}
		if (test < -0.499 * unit) { // singularity at south pole
			heading = -2 * Math.atan2(quaternion.getX(), quaternion.getW());
			attitude = -Math.PI / 2;
			bank = 0;
			return 0;
		}
		heading = Math.atan2(2 * quaternion.getY() * quaternion.getW() - 2
				* quaternion.getX() * quaternion.getZ(), sqx - sqy - sqz + sqw);
		attitude = Math.asin(2 * test / unit);
		bank = Math
				.atan2(2 * quaternion.getX() * quaternion.getW() - 2
						* quaternion.getY() * quaternion.getZ(), -sqx + sqy
						- sqz + sqw);

		return Math.toDegrees(heading);

	}

	/**
	 * Check if the given string is a float or not.
	 * 
	 * @param value
	 *            A string representing a float
	 * @return true if the string is a float, false otherwise.
	 */
	public static boolean isFloat(String value) {
		try {
			Float.parseFloat(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Utility used to update the quantity value shown in the UI.
	 * 
	 * @param qta
	 *            TextView to update
	 */
	public static int decrease(TextView qta) {

		try {
			int q = Integer.valueOf(qta.getText().toString());
			// decrease
			q -= 1;
			qta.setText("" + q);

			return q;
		} catch (Exception e) {
			return 0;
		}

	}

	/**
	 * Check inside the itinerary and return the default marker.
	 * 
	 * @param itinerary
	 *            list of markers
	 * @return default marker or null.
	 */
	public static NavigationMarker getDefaultMarker(List<NavigationMarker> itinerary) {

		for (NavigationMarker m : itinerary) {
			if (m.isDefaultMarker())
				return m;
		}

		return null;
	}
	
	/**
	 * Retirve the markerid from the config string.
	 * @param markerType
	 * @return
	 */
	public static int getIDfromType(String markerType) {
		String[] str = markerType.split(";");

		try {

			if (str[0].equals("single_barcode")) {
				int id = Integer.parseInt(str[1]);
				return id;
			}

		} catch (Exception e) {
			return -1;
		}

		return -1;
	}

}
