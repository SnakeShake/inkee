package it.inkee.ar.utils;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import it.inkee.ar.activity.navigator.InkeeNavigatorActivity;
import it.inkee.ar.activity.order.InkeeChooseOrder;
import it.inkee.ar.activity.wait.InkeeWaitOrdersActivity;

import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;

public class InkeeSpeechToText implements RecognitionListener {

	private static final String TAG = InkeeSpeechToText.class.getSimpleName();

	/**
	 * possibili parole:
	 * 
	 * comando inkee: parola chiave comando inkee -> ordini: visualizzare nuovo
	 * ordine comando inkee -> confermo: confermare ordine comando inkee ->
	 * articolo: visualizzare info articolo comando inkee -> scan
	 */

	/* Named searches allow to quickly reconfigure the decoder */
	private static final String KWS_SEARCH = "wakeup";
	private static final String MENU_SEARCH = "menu";

	/* Keyword we are looking for to activate menu */
	private static final String KEYPHRASE = "comando inkee";

	private SpeechRecognizer recognizer;
	private Context context;

	public InkeeSpeechToText(final Context context) {

		this.context = context;

		// Recognizer initialization is a time-consuming and it involves IO,
		// so we execute it in async task
		new AsyncTask<Void, Void, Exception>() {
			@Override
			protected Exception doInBackground(Void... params) {
				try {
					Assets assets = new Assets(context);
					File assetDir = assets.syncAssets();
					setupRecognizer(assetDir);
				} catch (IOException e) {
					Log.e(TAG, e.getMessage());
					return e;
				}
				return null;
			}

			@Override
			protected void onPostExecute(Exception result) {
				if (result != null) {
					Log.i(TAG, "Failed to init recognizer " + result);
				} else {
					switchSearch(KWS_SEARCH);
				}
			}
		}.execute();

	}

	private void setupRecognizer(File assetsDir) throws IOException {
		// The recognizer can be configured to perform multiple searches
		// of different kind and switch between them

		recognizer = defaultSetup()
				.setAcousticModel(new File(assetsDir, "en-us-ptm"))
				.setDictionary(new File(assetsDir, "cmudict-en-us.dict"))

				// To disable logging of raw audio comment out this call (takes
				// a lot of space on the device)
				.setRawLogDir(assetsDir)

				// Threshold to tune for keyphrase to balance between false
				// alarms and misses
				.setKeywordThreshold(1e-45f)

				// Use context-independent phonetic search, context-dependent is
				// too slow for mobile
				.setBoolean("-allphone_ci", true)

				.getRecognizer();
		recognizer.addListener(this);

		/**
		 * In your application you might not need to add all those searches.
		 * They are added here for demonstration. You can leave just one.
		 */

		// Create keyword-activation search.
		recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

		// Create grammar-based search for selection between demos
		File menuGrammar = new File(assetsDir, "menu.gram");
		recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);

		Log.i(TAG, "End init recognizer");

	}

	/**
	 * In partial result we get quick updates about current hypothesis. In
	 * keyword spotting mode we can react here, in other modes we need to wait
	 * for final result in onResult.
	 */
	@Override
	public void onPartialResult(Hypothesis hypothesis) {
		if (hypothesis == null)
			return;

		String text = hypothesis.getHypstr();

		if (text.equals(KEYPHRASE))
			switchSearch(MENU_SEARCH);

		Log.d(TAG, "Text: " + text);

	}

	/**
	 * This callback is called when we stop the recognizer.
	 */
	@Override
	public void onResult(Hypothesis hypothesis) {
		if (hypothesis != null) {
			String text = hypothesis.getHypstr();
			Log.i(TAG, "Text: " + text);

			/*
			 * send the data to the activity that has to manage the action
			 */
			sendData(text);

		}
	}

	public void sendData(String text) {

		Toast toast;

		if (text.equalsIgnoreCase(KEYPHRASE)) {
			toast = Toast.makeText(this.context, KEYPHRASE, Toast.LENGTH_SHORT);
		} else {
			Intent intent = sendIntent(text);
			if (intent.getAction() != null) {
				toast = Toast.makeText(this.context, text, Toast.LENGTH_SHORT);
				LocalBroadcastManager.getInstance(this.context).sendBroadcast(
						intent);
			} else {
				toast = Toast.makeText(this.context, "comando non valido",
						Toast.LENGTH_SHORT);
			}
		}

		//toast.setGravity(Gravity.BOTTOM | Gravity.END, 0, 0);
		toast.show();

	}

	private Intent sendIntent(String text) {

		Intent intent = new Intent();

		if (text.equals(InkeeUtils.INKEE_ORDERS_LIST)
				&& InkeeUtils.activity
						.equalsIgnoreCase(InkeeWaitOrdersActivity.class
								.getSimpleName())) {
			intent.setAction(InkeeUtils.INKEE_ACTION_ORDER);

		} else if (text.equals(InkeeUtils.INKEE_CONFIRMED)
				&& InkeeUtils.activity.equalsIgnoreCase(InkeeChooseOrder.class
						.getSimpleName())) {
			intent.setAction(InkeeUtils.INKEE_ACTION_ORDER_CONFIRMED);

		} else if (text.equals(InkeeUtils.INKEE_SCAN)
				&& InkeeUtils.activity
						.equalsIgnoreCase(InkeeNavigatorActivity.class
								.getSimpleName())) {
			intent.setAction(InkeeUtils.INKEE_ACTION_SCAN);

		} else if (text.equals(InkeeUtils.INKEE_ARTICLE)
				&& InkeeUtils.activity
						.equalsIgnoreCase(InkeeNavigatorActivity.class
								.getSimpleName())) {
			intent.setAction(InkeeUtils.INKEE_ACTION_SHOW_INFO);
		}

		return intent;
	}

	@Override
	public void onBeginningOfSpeech() {
	}

	/**
	 * We stop recognizer here to get a final result
	 */
	@Override
	public void onEndOfSpeech() {
		if (!recognizer.getSearchName().equals(KWS_SEARCH))
			switchSearch(KWS_SEARCH);
	}

	private void switchSearch(String searchName) {
		recognizer.stop();

		// If we are not spotting, start listening with timeout (10000 ms or 10
		// seconds).
		if (searchName.equals(KWS_SEARCH)) {
			recognizer.startListening(searchName);
			Log.d(TAG, "Assistente vocale pronto");
			// Toast.makeText(this.context, "Assistente vocale pronto",
			// Toast.LENGTH_SHORT).show();
		} else
			recognizer.startListening(searchName, 5000);
	}

	@Override
	public void onError(Exception error) {
		Log.e(TAG, error.getMessage());

	}

	@Override
	public void onTimeout() {
		switchSearch(KWS_SEARCH);
	}

	public void destroy() {
		recognizer.cancel();
		recognizer.shutdown();
	}

}
