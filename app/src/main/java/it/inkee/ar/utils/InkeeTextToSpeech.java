package it.inkee.ar.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;

/**
 * InkeeTextToSpeech synthesizes speech from text for immediate playback or to
 * create a sound file.
 * 
 * @author mutti
 * 
 */
public class InkeeTextToSpeech implements OnInitListener {

	private static final String TAG = InkeeTextToSpeech.class.getSimpleName();
	private TextToSpeech tts;
	private Context context;

	private boolean ttsReady = false;

	/**
	 * Initialize the {@link TextToSpeech} engine, and register an observer
	 * (mMessageReceiver) to receive Intents with actions named
	 * {@link InkeeUtils.INKEE_ACTION_SPEAK}.
	 * 
	 * @param context
	 *            Application context
	 */
	public InkeeTextToSpeech(Context context) {
		this.context = context;
		tts = new TextToSpeech(context, this);
		Log.i(TAG, "Initialize text to speech");

		LocalBroadcastManager.getInstance(context).registerReceiver(
				mMessageReceiver,
				new IntentFilter(InkeeUtils.INKEE_ACTION_SPEAK));

	}

	/**
	 * The handler for received Intents. This will be called whenever an Intent
	 * with an action named "INKEE_ACTION_SPEAK" is broadcasted.
	 */
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (InkeeUtils.INKEE_ACTION_SPEAK.equals(intent.getAction())) {
				String message = intent.getStringExtra("message");
				speak(message);
			}
		}
	};

	/**
	 * Called to signal the completion of the TextToSpeech engine
	 * initialization.
	 */
	@Override
	public void onInit(int initStatus) {
		if (initStatus == TextToSpeech.SUCCESS) {
			tts.setLanguage(Locale.ITALIAN);
			ttsReady = true;
			ready();
		} else if (initStatus == TextToSpeech.ERROR) {
			ttsReady = false;
			Toast.makeText(this.context,
					"Sorry! Text To Speech failed to start...",
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Log when the {@link TextToSpeech} engine is ready.
	 */
	private void ready() {
		// speak("Assistente vocale inizializzato.");
		Log.i(TAG, "Assistente vocale pronto all'uso");
	}

	/**
	 * This method allows to playback a given text.
	 * 
	 * @param text
	 *            Message to playback
	 */
	public void speak(String text) {
		if (ttsReady == true)
			tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
		else
			Toast.makeText(this.context, "TextToSpeech not initializated.",
					Toast.LENGTH_SHORT).show();
	}

	/**
	 * Used to pause the TextToSpeech.
	 */
	public void onPause() {
		tts.stop();
	}
}
