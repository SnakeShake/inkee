package it.inkee.ar.renderer;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.artoolkit.ar.base.ARToolKit;
import org.artoolkit.ar.base.NativeInterface;
import org.artoolkit.ar.base.rendering.ARRenderer;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import it.inkee.ar.marker.other.OtherMarker;
import it.inkee.ar.utils.InkeeUtils;

/**
 * 
 * InkeeRenderer allows to show to the user the path to follow in order to reach
 * the destination.
 * 
 * @author mutti
 * 
 */
public class InkeeRendererOther extends ARRenderer {

	private static final String TAG = InkeeRendererOther.class.getSimpleName();

	private Context context;

	private List<OtherMarker> allMarkers;

	private boolean showed = false;

	/**
	 * Initialize the class, and load the marker used as login.
	 * 
	 * @param context
	 *            Application context
	 * @param allMarkers
	 */
	public InkeeRendererOther(Context context, List<OtherMarker> allMarkers) {
		this.context = context;
		this.allMarkers = allMarkers;
	}

	/**
	 * This method is used to load the marker used in a phase that is not the
	 * navigation one.
	 */
	@Override
	public boolean configureARScene() {

		for (OtherMarker m : allMarkers) {

			if (m.getPhase() != null) {
				int marker = ARToolKit.getInstance().addMarker(m.getType());
				if (marker < 0)
					return false;

				Log.i(TAG, "Added marker with UID: " + marker);
			}

		}

		ARToolKit.getInstance().setPatternDetectionMode(
				NativeInterface.AR_MATRIX_CODE_DETECTION);

		Log.i(TAG, "switch to mode AR_MATRIX_CODE_DETECTION");

		ARToolKit.getInstance().setMatrixCodeType(
				NativeInterface.AR_MATRIX_CODE_3x3_HAMMING63);

		Log.i(TAG, "use 'AR_MATRIX_CODE_3x3_HAMMING63' markers");

		return true;
	}

	/**
	 * core method of the InkeeRenderer class. See inline comments.
	 */
	@Override
	public void draw(GL10 gl) {

		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

		// Apply the ARToolKit projection matrix
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity(); // reset the matrix to its default state

		gl.glLoadMatrixf(ARToolKit.getInstance().getProjectionMatrix(), 0);

		gl.glEnable(GL10.GL_CULL_FACE);
		gl.glShadeModel(GL10.GL_SMOOTH);
		gl.glEnable(GL10.GL_DEPTH_TEST);
		gl.glFrontFace(GL10.GL_CW);

		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity(); // reset the matrix to its default state

		gl.glDisable(GL10.GL_LIGHTING);

		// check wheter a login or scan marker is visible or not.
		OtherMarker m = whatToShow();
		if (m != null) {

			Intent intent = new Intent();
			if (m.getPhase().equalsIgnoreCase(InkeeUtils.LOGIN_PHASE)) {
				intent.setAction(InkeeUtils.INKEE_ACTION_LOGIN);
				intent.putExtra("user", m.getExtra());

				Log.d(TAG, "new used identified");

			} else if (m.getPhase().equalsIgnoreCase(InkeeUtils.SCAN_PHASE)) {
				intent.setAction(InkeeUtils.INKEE_ACTION_ITEM_SCANNED);
				intent.putExtra("quantita", "1"); // just for demo

				Log.d(TAG, "scanned product identified");
			}
			if (!showed)
				LocalBroadcastManager.getInstance(this.context).sendBroadcast(
						intent);
		}
	}

	/**
	 * This method checks whether a marker is visible or not.
	 * 
	 * @return the visible Marker
	 */
	private OtherMarker whatToShow() {
		// check if a marker is visible
		for (OtherMarker m : allMarkers) {
			if (ARToolKit.getInstance().queryMarkerVisibleWithPattID(
					m.getMarkerID())) {
				return m;
			}
		}

		return null;
	}
}
