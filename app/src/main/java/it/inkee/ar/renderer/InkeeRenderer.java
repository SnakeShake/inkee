package it.inkee.ar.renderer;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.artoolkit.ar.base.ARToolKit;
import org.artoolkit.ar.base.NativeInterface;
import org.artoolkit.ar.base.rendering.ARRenderer;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import it.inkee.ar.marker.path.NavigationMarker;
import it.inkee.ar.sensor.provider.OrientationProvider;
import it.inkee.ar.utils.InkeeUtils;
import it.inkee.ar.utils.Rotation;

/**
 * 
 * InkeeRenderer allows to show to the user the path to follow in order to reach
 * the destination.
 * 
 * @author mutti
 * 
 */
public class InkeeRenderer extends ARRenderer {

	private static final String TAG = InkeeRenderer.class.getSimpleName();

	private boolean isInit = false;
	private boolean readyToTurn = false;
	private float pivot = 0;

	private NavigationMarker lastMarker;
	private int countDelay = 0;

	/**
	 * get data from sensors
	 */
	private OrientationProvider orientationProvider;

	private Context context;

	private List<NavigationMarker> itinerary;

	final Handler handler = new Handler();

	/**
	 * Initialize the class, and load the models to draw.
	 * 
	 * @param oP
	 *            used to retrieve data from the sensors
	 * @param context
	 *            Application context
	 * @param itinerary
	 */
	public InkeeRenderer(OrientationProvider oP, Context context,
			List<NavigationMarker> itinerary) {
		this.orientationProvider = oP;
		this.context = context;
		this.itinerary = itinerary;
	}

	/**
	 * This method is used to load the markers used in the path.
	 */
	@Override
	public boolean configureARScene() {

		for (NavigationMarker m : itinerary) {

			int marker = ARToolKit.getInstance().addMarker(m.getType());
			if (marker < 0)
				return false;

			Log.i(TAG, "Added marker with UID: " + marker);

		}

		ARToolKit.getInstance().setPatternDetectionMode(
				NativeInterface.AR_MATRIX_CODE_DETECTION);

		Log.i(TAG, "switch to mode AR_MATRIX_CODE_DETECTION");

		ARToolKit.getInstance().setMatrixCodeType(
				NativeInterface.AR_MATRIX_CODE_3x3_HAMMING63);

		Log.i(TAG, "use 'AR_MATRIX_CODE_3x3_HAMMING63' markers");

		return true;
	}

	/**
	 * core method of the InkeeRenderer class. See inline comments.
	 */
	@Override
	public void draw(GL10 gl) {

		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

		// Apply the ARToolKit projection matrix
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity(); // reset the matrix to its default state

		gl.glLoadMatrixf(ARToolKit.getInstance().getProjectionMatrix(), 0);

		gl.glEnable(GL10.GL_CULL_FACE);
		gl.glShadeModel(GL10.GL_SMOOTH);
		gl.glEnable(GL10.GL_DEPTH_TEST);
		gl.glFrontFace(GL10.GL_CW);

		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity(); // reset the matrix to its default state

		gl.glDisable(GL10.GL_LIGHTING);

		// trick just wait a little bit and then manage the turn
		if (readyToTurn) {
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					manageTurn();
				}
			}, 1000);
		}

		// check wheter a marker is visible or not.
		NavigationMarker m = whatToShow();
		if (m != null) {

			gl.glMatrixMode(GL10.GL_MODELVIEW);
			gl.glLoadIdentity(); // reset the matrix to its default state

			gl.glPushMatrix();

			if (m.getTransformationMatrix() != null)
				gl.glLoadMatrixf(m.getTransformationMatrix(), 0);
			else
				gl.glLoadMatrixf(ARToolKit.getInstance()
						.queryMarkerTransformationWithPattID(m.getMarkerID()),
						0);

			// retrieve the distance between the camera and the marker
			float zDist = 0;
			try {

				zDist = ARToolKit.getInstance()
						.queryMarkerCameraTransformation(m.getMarkerID())[11];

				// check if the camera is "near" (< 1m) or not
				if ((zDist / 1000) < 1.0) {
					m.setNear(true);
				} else
					m.setNear(false);

				// update the textview
				if (!m.isDefaultMarker())
					updateTextView(zDist);

			} catch (NullPointerException e) {
				// no zDist for marker default
				// or user is too fast (move head around)
				updateTextView(0);
				if (m.isEnd())
					m.setNear(false);
			} finally {

				/**
				 * based on the info read from the json apply a set of
				 * transformation.
				 */

				// apply base transformation
				m.applyTransformation(gl, m.getTransformations());

				// this should be used for the default marker
				if (m.needOrientation()) {
					gl.glPushMatrix();
					/* Apply transformation from sensor data */
					gl.glMultMatrixf(orientationProvider.getRotationMatrix(), 0);
				}

				// apply additional transformation
				m.applyTransformation(gl, m.getAdditionalTransformations());

				if (m.getScale() != null) {
					gl.glPushMatrix(); // glscalef
					gl.glScalef(m.getScale().getX(), m.getScale().getY(), m
							.getScale().getZ());
				}

				gl.glPushMatrix(); // draw

				// draw
				if (m.isNear()) {
					gl.glFrontFace(m.isNearClockwise() ? GL10.GL_CW
							: GL10.GL_CCW);
					m.getNearModel().Draw();
					if (!m.isDefaultMarker()) {
						if (!m.isEnd()) {
							// ready to turn
							pivot = (float) InkeeUtils
									.getHeading(orientationProvider
											.getQuaternion());
							readyToTurn = true;
						} else {
							Intent intent = new Intent();
							intent.setAction(InkeeUtils.INKEE_ACTION_SCAN);
							LocalBroadcastManager.getInstance(this.context)
									.sendBroadcast(intent);
						}
					}
				} else {
					gl.glFrontFace(m.isFarClockwise() ? GL10.GL_CW
							: GL10.GL_CCW);
					m.getFarModel().Draw();
					readyToTurn = false;
				}

				gl.glPopMatrix(); // draw

				if (m.getScale() != null)
					gl.glPopMatrix(); // glscalef

				if (m.needOrientation())
					gl.glPopMatrix();

				// just pop everything
				for (int i = 0; i < m.getAdditionalTransformations().size(); i++)
					gl.glPopMatrix();

				// just pop everything
				for (int i = 0; i < m.getTransformations().size(); i++)
					gl.glPopMatrix();

				gl.glPopMatrix(); // glLoadMatrixf
			}
		}
	}

	/**
	 * This method allows to check if the user turned the head. If yes, then
	 * apply a transformation to the default marker, if not do nothing.
	 */
	private void manageTurn() {

		float cAngle = (float) InkeeUtils.getHeading(orientationProvider
				.getQuaternion());

		Log.i(TAG, "ready to turn");

		if (isTurned(cAngle)) {
			NavigationMarker defaultMarker = InkeeUtils
					.getDefaultMarker(itinerary);
			defaultMarker.clearAdditionalTransformations();

			// compensate
			Rotation r0 = new Rotation(-cAngle, 0, 0, 1);
			defaultMarker.addAdditionalTransformation(r0);

			// rotate the object
			Rotation r1 = new Rotation(-90, 0, 0, 1);
			defaultMarker.addAdditionalTransformation(r1);

			readyToTurn = false;
		}
	}

	/**
	 * This method allows to check if the user turn the head at least of 70
	 * degree (i.e., change walk direction).
	 * 
	 * @param cAngle
	 * @return true if the user changed walk direction, false otherwise
	 */
	private boolean isTurned(float cAngle) {

		float rotation = Math.abs(cAngle - pivot);
		Log.d(TAG, "absolute rotation of: " + rotation + " degree");

		if (lastMarker == null)
			return true;

		return false;
	}

	/**
	 * This method checks whether a marker is visible or not.
	 * 
	 * @return the visible Marker
	 */
	private NavigationMarker whatToShow() {
		// check if a marker is visible
		for (NavigationMarker m : itinerary) {

			// if (ARToolKit.getInstance().queryMarkerVisibleWithPattID(
			// m.getMarkerID())) {
			// Log.i("RendererTest", "visible marker: " + m.getMarkerID());
			// }

			if (ARToolKit.getInstance().queryMarkerVisibleWithPattID(
					m.getMarkerID())) {
				if (m.isDefaultMarker())
					isInit = initDefault(m);
				else {
					countDelay = 0;
					lastMarker = m;
				}

				// end of the path, do not show anymore the default arrow
				if (m.isEnd())
					isInit = false;

				return m;
			}
		}

		// wait and show the last visible marker
		if (lastMarker != null && !lastMarker.isDefaultMarker()
				&& lastMarker.getTransformationMatrix() != null) {
			NavigationMarker tmp = delay();
			if (tmp != null)
				return tmp;
		}

		// return default
		if (isInit && !readyToTurn)
			return InkeeUtils.getDefaultMarker(itinerary);
		else
			return null;
	}

	private NavigationMarker delay() {

		this.countDelay++;

		if (countDelay == InkeeUtils.DELAY) {
			countDelay = 0;
			lastMarker = null;
		}

		return lastMarker;

	}

	// private boolean delay(boolean ready) {
	//
	// if (ready || countDelay == InkeeUtils.delay) {
	// countDelay = 0;
	// return true;
	// }
	//
	// countDelay++;
	// return false;
	// }

	/**
	 * the Marker 0 or default Marker is a special Marker that needs a special
	 * initialization.
	 * 
	 * @param mDefault
	 *            default Marker
	 * @return true if the initialization was successfully completed, false
	 *         otherwise
	 */
	private boolean initDefault(NavigationMarker mDefault) {

		float heading = (float) InkeeUtils.getHeading(orientationProvider
				.getQuaternion());

		if (!mDefault.getAdditionalTransformations().isEmpty())
			mDefault.clearAdditionalTransformations();

		// maintain two separate transformation, easy to debug
		// go to zero degree
		Rotation r0 = new Rotation(-heading, 0, 0, 1);
		mDefault.addAdditionalTransformation(r0);

		// rotate the object
		Rotation r1 = new Rotation(-90, 0, 0, 1);
		mDefault.addAdditionalTransformation(r1);

		return true;

	}

	/**
	 * When the marker is visible, this method allows to update the textview
	 * showing the distance between the marker and the camera.
	 * 
	 * @param f
	 *            distance in meter
	 */
	private void updateTextView(float f) {

		Intent intent = new Intent(InkeeUtils.INKEE_ACTION_UPDATE_DISTANCE);
		intent.putExtra("distance", "" + f);

		Log.d(TAG, "update distance. New value = " + f);

		LocalBroadcastManager.getInstance(this.context).sendBroadcast(intent);

	}

	/**
	 * Remove markers.
	 */
	public void removeAllMarkers() {

		int removed = ARToolKit.getInstance().removeAllMarkers();
		Log.i(TAG, "Removed " + removed + " markers");

	}
}
