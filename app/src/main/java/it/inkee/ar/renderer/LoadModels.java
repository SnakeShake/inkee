package it.inkee.ar.renderer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import nz.gen.geek_central.GLUseful.ObjReader;
import nz.gen.geek_central.GLUseful.ObjReader.Model;
import android.content.res.AssetManager;
import android.util.Log;

public class LoadModels {

	private static final String TAG = LoadModels.class.getSimpleName();

	HashMap<String, Model> map = new HashMap<String, Model>();

	public LoadModels() {

	}

	public void load(AssetManager am, String dirFrom) {

		String fileList[];
		try {
			fileList = am.list(dirFrom);

			if (fileList != null) {
				for (int i = 0; i < fileList.length; i++) {
					if (fileList[i].endsWith(".obj")) {

						InputStream isObj = am.open(dirFrom + File.separator
								+ fileList[i]);
						InputStream isMaterial = am.open(dirFrom
								+ File.separator
								+ fileList[i].replace(".obj", ".mtl"));
						Model m = readObj(isObj, isMaterial);

						map.put(fileList[i].substring(0,
								fileList[i].lastIndexOf('.')), m);
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public HashMap<String, Model> getModels() {
		return map;
	}

	public ObjReader.Model readObj(final InputStream objInStream,
			final InputStream matInStream) {
		ObjReader.Model Result = null;
		try {
			Result = ObjReader.ReadObj(
			/* FileName = */objInStream,
			/* LoadMaterials = */
			new ObjReader.MaterialLoader() {
				public ObjReader.MaterialSet Load(
						ObjReader.MaterialSet Materials, String MatFileName) {
					return ObjReader.ReadMaterials(matInStream, Materials);
				} /* Load */
			} /* MaterialLoader */
			);
		} catch (ObjReader.DataFormatException Failed) {
			Log.e(TAG, Failed.toString());
		} /* try */
		return Result;
	} /* ReadObj */

}
