package it.inkee.ar.activity.barcode;//package it.inkee.activity.barcode;
//
//import it.inkee.activity.login.InkeeLoginActivity;
//import it.inkee.activity.scan.InkeeScanActivity;
//import it.inkee.utils.InkeeUtils;
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.content.LocalBroadcastManager;
//import android.util.Log;
//
//import com.google.zxing.Result;
//import com.welcu.android.zxingfragmentlib.BarCodeScannerFragment;
//
///**
// * 
// * @author mutti
// * 
// */
//public class BarcodeFragment extends BarCodeScannerFragment {
//
//	private static final String TAG = BarcodeFragment.class.getSimpleName();
//
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		this.setmCallBack(new IResultCallback() {
//			@Override
//			public void result(Result lastResult) {
//
//				Intent intent = new Intent();
//				String activityName = getActivity().getClass().getSimpleName();
//
//				if (activityName.equals(InkeeLoginActivity.class
//						.getSimpleName())) {
//					intent.setAction(InkeeUtils.INKEE_ACTION_LOGIN);
//					intent.putExtra("user", lastResult.toString());
//
//					Log.d(TAG, "new used identified");
//
//				} else if (activityName.equals(InkeeScanActivity.class
//						.getSimpleName())) {
//
//					intent.setAction(InkeeUtils.INKEE_ACTION_ITEM_SCANNED);
//					intent.putExtra("quantita", "1"); // just for demo
//
//					Log.d(TAG, "scanned product identified");
//
//				}
//
//				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
//						intent);
//
//			}
//		});
//	}
//
//	public BarcodeFragment() {
//
//	}
//
//	@Override
//	public int getRequestedCameraId() {
//		return -1; // set to 1 to use the front camera (won't work if the device
//					// doesn't have one, it is up to you to handle this method
//					// ;)
//	}
//}