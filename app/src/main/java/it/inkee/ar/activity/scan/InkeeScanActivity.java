package it.inkee.ar.activity.scan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.artoolkit.ar.base.ARActivity;
import org.artoolkit.ar.base.rendering.ARRenderer;

import it.inkee.ar.R;
import it.inkee.ar.activity.end.InkeeEndOrderActivity;
import it.inkee.ar.activity.login.InkeeLoginActivity;
import it.inkee.ar.activity.order.Order;
import it.inkee.ar.application.InkeeApplication;
import it.inkee.ar.renderer.InkeeRendererOther;
import it.inkee.ar.utils.InkeeUtils;
import jp.epson.moverio.bt200.SensorControl;

/**
 * InkeeScanActivity allows to scan the QRcode of the picked product.
 * 
 * @author mutti
 * 
 */
public class InkeeScanActivity extends ARActivity {

	private static final String TAG = InkeeLoginActivity.class.getSimpleName();

	Handler handler = new Handler();
	private boolean scanned = false;

	/**
	 * Layout object used to show the camera preview and the opengl objects.
	 */
	private FrameLayout cameraLayout;
	private FrameLayout glviewLayout;
	private GLSurfaceView gl2View;
	private InkeeRendererOther inkeeRendererOther;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		InkeeUtils.moverioScreenCustomization(this,
				SensorControl.SENSOR_MODE_HEADSET, getWindow(), 10, false);

		setContentView(R.layout.activity_inkee_scan);

		getWindow().getDecorView().setBackgroundColor(Color.BLACK);

	}

	/**
	 * When resume the app, register an observer (mMessageReceiver) to receive
	 * Intents with actions named {@link InkeeUtils.INKEE_ACTION_ITEM_SCANNED}.
	 */
	@Override
	public void onResume() {
		super.onResume();

		LocalBroadcastManager.getInstance(this).registerReceiver(
				mMessageReceiver,
				new IntentFilter(InkeeUtils.INKEE_ACTION_ITEM_SCANNED));

		Log.d(TAG, "LocalBroadcast for the action "
				+ InkeeUtils.INKEE_ACTION_ITEM_SCANNED
				+ " succesfully registered");

		TextView qta = (TextView) findViewById(R.id.scanQtaValue);
		TextView article = (TextView) findViewById(R.id.scanArticleValue);
		TextView description = (TextView) findViewById(R.id.scanDescriptionValue);

		Order o = (Order) getIntent().getSerializableExtra("order");
		if (o != null) {
			article.setText(o.getCodArticle());
			description.setText(o.getDescription());
			qta.setText("   " + o.getQta() + "   ");

			qta.setBackgroundColor(Color.YELLOW);

		}

	}

	@Override
	protected ARRenderer supplyRenderer() {
		Log.d(TAG, "Create Renderer object");

		inkeeRendererOther = new InkeeRendererOther(getApplicationContext(),
				((InkeeApplication) getApplication()).getOtherMarkers());

		return inkeeRendererOther;
	}

	@Override
	protected FrameLayout supplyCameraLayout() {
		cameraLayout = (FrameLayout) this.findViewById(R.id.cameraLayoutScan);
		if( ((InkeeApplication) getApplication()).isInDemoMode() ){
			cameraLayout.setLayoutParams(new ViewGroup.LayoutParams(-1,-1));
		}
		Log.d(TAG, "Camera layout supplied");
		return cameraLayout;
	}

	@Override
	protected FrameLayout supplyGLViewLayout() {
		glviewLayout = (FrameLayout) this.findViewById(R.id.glviewLayoutScan);
		Log.d(TAG, "GLView layout supplied");
		return glviewLayout;
	}

	@Override
	protected GLSurfaceView supplyGLView() {
		return gl2View;
	}

	/**
	 * When pause the app, unregister the observer.
	 */
	@Override
	protected void onPause() {
		super.onPause();

		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				mMessageReceiver);

		Log.d(TAG, "LocalBroadcast unregistered");

	}

	/**
	 * The handler for received Intents. This will be called whenever an Intent
	 * with an action named "INKEE_ACTION_ITEM_SCANNED" is broadcasted.
	 */
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (InkeeUtils.INKEE_ACTION_ITEM_SCANNED.equals(intent.getAction())
					&& !scanned) {

				scanned = true;

				TextView qta = (TextView) findViewById(R.id.scanQtaValue);
				TextView article = (TextView) findViewById(R.id.scanArticleValue);
				TextView description = (TextView) findViewById(R.id.scanDescriptionValue);

				qta.setBackgroundColor(Color.GREEN);
				qta.setText("   " + InkeeUtils.decrease(qta) + "   ");
				article.setText("---");
				description.setText("---");

				String sms = "prodotto scansionato correttamente";

				Intent i = new Intent(InkeeUtils.INKEE_ACTION_SPEAK);
				i.putExtra("message", sms);
				LocalBroadcastManager.getInstance(getApplicationContext())
						.sendBroadcast(i);

				Log.d(TAG, "product picked and scanned correctly");

				handler.postDelayed(new Runnable() {
					@Override
					public void run() {

						finishPickup();
						Log.d(TAG, "call and acrivity");

					}
				}, 5000);
			}
		}
	};

	/**
	 * This method allows to notify to the user that the pickup phase is ended
	 * and start the "resume order" activity.
	 */
	private void finishPickup() {

		Intent i0 = new Intent(InkeeUtils.INKEE_ACTION_SPEAK);
		i0.putExtra("message", "Tutti i prodotti sono stati prelevati");
		LocalBroadcastManager.getInstance(getApplicationContext())
				.sendBroadcast(i0);

		Intent i1 = new Intent(this, InkeeEndOrderActivity.class);
		startActivity(i1);

		Log.d(TAG, "start InkeeEndOrderActivity activity");

	}
}
