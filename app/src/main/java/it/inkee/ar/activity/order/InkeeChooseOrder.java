package it.inkee.ar.activity.order;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.ListView;

import java.util.List;

import it.inkee.ar.R;
import it.inkee.ar.activity.navigator.InkeeNavigatorActivity;
import it.inkee.ar.utils.InkeeUtils;
import jp.epson.moverio.bt200.SensorControl;

/**
 * InkeeChooseOrder allows the user to accept or reject an order.
 * 
 * @author mutti
 * 
 */
public class InkeeChooseOrder extends Activity {

	private static final String TAG = InkeeChooseOrder.class.getSimpleName();

	ListView listView;

	private ListAdapter adapterLeft;
	private List<Order> orders;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		InkeeUtils.moverioScreenCustomization(this,
				SensorControl.SENSOR_MODE_HEADSET, getWindow(), 15, false);

		setContentView(R.layout.activity_inkee_choose_order);

		getWindow().getDecorView().setBackgroundColor(Color.BLACK);

	}

	/**
	 * When resume the app retrieve the order and register an observer
	 * (mMessageReceiver) to receive Intents with actions named
	 * {@link InkeeUtils.INKEE_ACTION_ORDER_CONFIRMED}.
	 */
	@Override
	protected void onResume() {
		super.onResume();

		// retrieve the new order
		LoadOrder lo = new LoadOrder();
		lo.load(getApplicationContext());

		orders = lo.getOrders();

		adapterLeft = new ListAdapter(this, R.layout.activity_inkee_listview,
				orders);

		ListView listViewLeft = (ListView) findViewById(R.id.listView);
		listViewLeft.setAdapter(adapterLeft);

		LocalBroadcastManager.getInstance(this).registerReceiver(
				mMessageReceiver,
				new IntentFilter(InkeeUtils.INKEE_ACTION_ORDER_CONFIRMED));

		Log.d(TAG, "LocalBroadcast for the action "
				+ InkeeUtils.INKEE_ACTION_ORDER_CONFIRMED
				+ " succesfully registered");

		InkeeUtils.setActivityVisible(InkeeChooseOrder.class.getSimpleName());

	}

	/**
	 * When pause the app, unregister the observer.
	 */
	@Override
	protected void onPause() {
		super.onPause();

		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				mMessageReceiver);

		Log.d(TAG, "LocalBroadcast unregistered");

	}

	/**
	 * The handler for received Intents. This will be called whenever an Intent
	 * with an action named "INKEE_ACTION_ORDER_CONFIRMED" is broadcasted.
	 */
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (InkeeUtils.INKEE_ACTION_ORDER_CONFIRMED.equals(intent
					.getAction())) {

				Log.i(TAG, "order confirmed");

				// TODO need to use the id of the table shown to the user

				Order o = orders.get(0);
				// change the UI (i.e., set the background to Color.GREEN)
				o.setAccepted(true);
				adapterLeft.notifyDataSetChanged();

				startInkeeNavigatorActivity(o);

			}
		}
	};

	/**
	 * This method allows to start the InkeeNavigatorActivity activity.
	 * 
	 * @param acceptedOrder
	 *            object representing the order accepted by the user
	 */
	public void startInkeeNavigatorActivity(Order acceptedOrder) {

		Intent i = new Intent(getApplicationContext(),
				InkeeNavigatorActivity.class);
		i.putExtra("order", acceptedOrder);
		startActivity(i);

	}

}
