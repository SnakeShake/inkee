package it.inkee.ar.activity.navigator;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.artoolkit.ar.base.ARActivity;
import org.artoolkit.ar.base.rendering.ARRenderer;

import java.text.DecimalFormat;
import java.util.HashMap;

import it.inkee.ar.R;
import it.inkee.ar.activity.order.Order;
import it.inkee.ar.activity.scan.InkeeScanActivity;
import it.inkee.ar.application.InkeeApplication;
import it.inkee.ar.renderer.InkeeRenderer;
import it.inkee.ar.sensor.provider.OrientationProvider;
import it.inkee.ar.sensor.provider.RotationVectorProvider;
import it.inkee.ar.utils.InkeeUtils;
import jp.epson.moverio.bt200.SensorControl;
import nz.gen.geek_central.GLUseful.ObjReader.Model;

/**
 * InkeeNavigatorActivity allows to drive the user to the product location.
 * 
 * @author mutti
 * 
 */
public class InkeeNavigatorActivity extends ARActivity {

	private static final String TAG = InkeeNavigatorActivity.class
			.getSimpleName();

	/**
	 * Layout object used to show the camera preview and the opengl objects.
	 */
	private FrameLayout cameraLayout;
	private FrameLayout glviewLayout;
	private GLSurfaceView gl2View;
	private InkeeRenderer inkeeRenderer;

	HashMap<String, Model> mapModels;

	/**
	 * get data from sensors
	 */
	private OrientationProvider currentOrientationProvider;

	private TextView near;
	Handler handler = new Handler();

	Animation fadeIn;
	Animation fadeOut;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		InkeeUtils.moverioScreenCustomization(this,
				SensorControl.SENSOR_MODE_HEADSET, getWindow(), 10, false);

		setContentView(R.layout.activity_inkee_navigator);

		getWindow().getDecorView().setBackgroundColor(Color.BLACK);

		currentOrientationProvider = new RotationVectorProvider(
				(SensorManager) getSystemService(SENSOR_SERVICE));

		near = (TextView) findViewById(R.id.textViewNear);

		fadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_in);

		fadeOut = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.fade_out);

	}

	/**
	 * The handler for received Intents. This will be called whenever an Intent
	 * with an action named "INKEE_ACTION_SCAN" or
	 * "INKEE_ACTION_UPDATE_DISTANCE" is broadcasted.
	 */
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (InkeeUtils.INKEE_ACTION_SCAN.equals(intent.getAction())) {
				Log.i(TAG, "start scan activity");
				startScanActivity();
			} else if (InkeeUtils.INKEE_ACTION_UPDATE_DISTANCE.equals(intent
					.getAction())) {
				changeNearValue((String) intent.getExtras().get("distance"));
			} else if (InkeeUtils.INKEE_ACTION_SHOW_INFO.equals(intent
					.getAction())) {
				showUbication(false);
			}
		}
	};

	/**
	 * When the app is resumed, use polling to the get data from the orientation
	 * sensor, and show the ubication to the user. Furthermore, the activity and
	 * registers an observer (mMessageReceiver) to receive Intents with actions
	 * named {@link InkeeUtils.INKEE_ACTION_SCAN} or
	 * {@link InkeeUtils.INKEE_ACTION_UPDATE_DISTANCE}.
	 */
	@Override
	public void onResume() {
		super.onResume();

		currentOrientationProvider.start();

		Log.d(TAG, "orientation provider successffully started");

		IntentFilter filter = new IntentFilter(InkeeUtils.INKEE_ACTION_SCAN);
		filter.addAction(InkeeUtils.INKEE_ACTION_SHOW_INFO);
		filter.addAction(InkeeUtils.INKEE_ACTION_UPDATE_DISTANCE);
		LocalBroadcastManager.getInstance(this).registerReceiver(
				mMessageReceiver, filter);

		Log.d(TAG, "LocalBroadcast for the actions "
				+ InkeeUtils.INKEE_ACTION_SCAN + ", "
				+ InkeeUtils.INKEE_ACTION_UPDATE_DISTANCE + " and "
				+ InkeeUtils.INKEE_ACTION_SHOW_INFO + " succesfully registered");

		// show ubication
		TextView tvUbicationValue = (TextView) findViewById(R.id.textUbication);
		TextView tvCodArticleTitle = (TextView) findViewById(R.id.articleTitle);
		TextView tvCodArticleValue = (TextView) findViewById(R.id.articleValue);
		TextView tvDescriptionTitle = (TextView) findViewById(R.id.descriptionTitle);
		TextView tvDescriptionValue = (TextView) findViewById(R.id.descriptionValue);
		TextView tvQtaTitle = (TextView) findViewById(R.id.qtaNavTitle);
		TextView tvQtaValue = (TextView) findViewById(R.id.qtaNavValue);

		Order o = (Order) getIntent().getSerializableExtra("order");

		if (o != null) {
			tvUbicationValue.setText(o.getUbication());

			tvCodArticleValue.setText(o.getCodArticle());
			tvDescriptionValue.setText(o.getDescription());
			tvQtaValue.setText(o.getQta());
		}else{
			tvUbicationValue.setText("001-002-001-001-004"); //just for debug
		}

		// Animation
		// tvCodArticleTitle.startAnimation(fadeIn);
		// tvCodArticleValue.startAnimation(fadeIn);
		// tvDescriptionTitle.startAnimation(fadeIn);
		// tvDescriptionValue.startAnimation(fadeIn);
		// tvQtaTitle.startAnimation(fadeIn);
		// tvQtaValue.startAnimation(fadeIn);

		handler.postDelayed(new Runnable() {
			@Override
			public void run() {

				showUbication(true);
				Log.d(TAG, "show ubication information");

			}
		}, 10000);

		InkeeUtils.setActivityVisible(InkeeNavigatorActivity.class
				.getSimpleName());
		
		Intent intent = new Intent(InkeeUtils.INKEE_ACTION_SPEAK);
		intent.putExtra("message", "Inizio navigazione");
		LocalBroadcastManager.getInstance(getApplicationContext())
				.sendBroadcast(intent);

	}

	@Override
	protected ARRenderer supplyRenderer() {
		Log.d(TAG, "Create Renderer object");
		inkeeRenderer = new InkeeRenderer(currentOrientationProvider,
				getApplicationContext(), ((InkeeApplication) getApplication()).getNavigationMarkers());

		

		return inkeeRenderer;
	}

	@Override
	protected FrameLayout supplyCameraLayout() {

		cameraLayout = (FrameLayout) this.findViewById(R.id.cameraLayout);
		// If we're in demo mode we'll show the whole camera preview so we can cast the screen
		if( ((InkeeApplication) getApplication()).isInDemoMode() ){
		     cameraLayout.setLayoutParams(new ViewGroup.LayoutParams(-1,-1));
        }
		Log.d(TAG, "Camera layout supplied");
		return cameraLayout;
	}

	@Override
	protected FrameLayout supplyGLViewLayout() {
		glviewLayout = (FrameLayout) this.findViewById(R.id.glviewLayout);
		Log.d(TAG, "GLView layout supplied");
		return glviewLayout;
	}

	@Override
	protected GLSurfaceView supplyGLView() {
		return gl2View;
	}

	/**
	 * This method allows to update the textView representing the distance
	 * between the user and the marker. Different colors are used to identify
	 * range of values: green if distance is less than 1m, red otherwise. If the
	 * value is zero than hide the TextView.
	 * 
	 * @param value
	 *            updated value
	 */
	private void changeNearValue(final String value) {
		near.post(new Runnable() {
			public void run() {
				float newNear = Float.parseFloat(value) / 1000;

				DecimalFormat df = new DecimalFormat();/*
														 * convert in a readable
														 * number
														 */
				df.setMaximumFractionDigits(1);
				near.setText(df.format(newNear) + "m");
				if (newNear == 0)
					near.setTextColor(Color.BLACK);
				else if (newNear < 1)
					near.setTextColor(Color.GREEN);
				else
					near.setTextColor(Color.RED);
			}
		});

	}

	/**
	 * This method allows to start the InkeeScanActivity. A parameter
	 * representing the order to manage is pased to the InkeeScanActivity.
	 */
	public void startScanActivity() {
		Intent intent = new Intent(getApplicationContext(),
				InkeeScanActivity.class);

		Intent iSms = new Intent(InkeeUtils.INKEE_ACTION_SPEAK);
		Order o = (Order) getIntent().getSerializableExtra("order");
		if (o != null) {
			intent.putExtra("order", o);
			startActivity(intent);

			intent.putExtra("message", "Scansione articolo attivata");
			LocalBroadcastManager.getInstance(getApplicationContext())
					.sendBroadcast(iSms);

			Log.d(TAG, "start InkeeScanActivity activity");

		} else
			Log.e(TAG, "unable to deserialize the Order object.");
	}

	/**
	 * This method allows to show the ubication or general info about the order
	 * to manage.
	 * 
	 * @param showUbication
	 *            true if you want to show the ubication, false otherwise
	 */
	private void showUbication(boolean showUbication) {

		TextView tvCodArticleTitle = (TextView) findViewById(R.id.articleTitle);
		TextView tvDescriptionTitle = (TextView) findViewById(R.id.descriptionTitle);
		TextView tvQtaTitle = (TextView) findViewById(R.id.qtaNavTitle);

		TextView tvUbication = (TextView) findViewById(R.id.textUbication);
		TextView tvCodArticle = (TextView) findViewById(R.id.articleValue);
		TextView tvDescription = (TextView) findViewById(R.id.descriptionValue);
		TextView tvQta = (TextView) findViewById(R.id.qtaNavValue);

		tvUbication.setVisibility(View.VISIBLE);

		if (showUbication) {

			near.setVisibility(View.VISIBLE);

			tvCodArticleTitle.setVisibility(View.INVISIBLE);
			tvDescriptionTitle.setVisibility(View.INVISIBLE);
			tvQtaTitle.setVisibility(View.INVISIBLE);

			tvCodArticle.setVisibility(View.INVISIBLE);
			tvDescription.setVisibility(View.INVISIBLE);
			tvQta.setVisibility(View.INVISIBLE);

		} else {

			near.setVisibility(View.INVISIBLE);

			tvCodArticleTitle.setVisibility(View.VISIBLE);
			tvDescriptionTitle.setVisibility(View.VISIBLE);
			tvQtaTitle.setVisibility(View.VISIBLE);

			tvCodArticle.setVisibility(View.VISIBLE);
			tvDescription.setVisibility(View.VISIBLE);
			tvQta.setVisibility(View.VISIBLE);

			handler.postDelayed(new Runnable() {
				@Override
				public void run() {

					showUbication(true);
					Log.d(TAG, "show ubication information");

				}
			}, 5000);

		}

	}
}
