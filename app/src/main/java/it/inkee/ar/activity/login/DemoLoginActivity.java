package it.inkee.ar.activity.login;

import android.os.Bundle;

import it.inkee.ar.application.InkeeApplication;

public class DemoLoginActivity extends InkeeLoginActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {

        ((InkeeApplication) getApplication()).setDemoMode();

        super.onCreate(savedInstanceState);
    }
}
