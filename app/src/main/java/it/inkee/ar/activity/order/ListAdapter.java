package it.inkee.ar.activity.order;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import it.inkee.ar.R;

public class ListAdapter extends ArrayAdapter<Order> {

	public ListAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	public ListAdapter(Context context, int resource, List<Order> items) {
		super(context, resource, items);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;

		if (v == null) {
			LayoutInflater vi;
			vi = LayoutInflater.from(getContext());
			v = vi.inflate(R.layout.activity_inkee_listview, null);
		}

		Order o = getItem(position);

		if (o != null) {
			TextView tvOrderNumber = (TextView) v
					.findViewById(R.id.orderNumberValue);
			TextView tvClient = (TextView) v.findViewById(R.id.clientValue);
			TextView tvQta = (TextView) v.findViewById(R.id.qtaValue);

			if (tvOrderNumber != null)
				tvOrderNumber.setText(o.getOrderNumber());

			if (tvClient != null)
				tvClient.setText(o.getClient());

			if (tvQta != null)
				tvQta.setText(o.getQta());

			if (o.getAccepted()) {
				tvOrderNumber.setBackgroundColor(Color.GREEN);
				tvClient.setBackgroundColor(Color.GREEN);
				tvQta.setBackgroundColor(Color.GREEN);

			} else {
				tvOrderNumber.setBackgroundColor(Color.YELLOW);
				tvClient.setBackgroundColor(Color.YELLOW);
				tvQta.setBackgroundColor(Color.YELLOW);
			}
		}

		return v;
	}

}