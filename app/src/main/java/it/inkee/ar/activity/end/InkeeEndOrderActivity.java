package it.inkee.ar.activity.end;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import it.inkee.ar.R;
import it.inkee.ar.activity.login.InkeeLoginActivity;
import it.inkee.ar.activity.wait.InkeeWaitOrdersActivity;
import it.inkee.ar.sensor.provider.ShakeDetector;
import it.inkee.ar.utils.InkeeUtils;
import jp.epson.moverio.bt200.SensorControl;

public class InkeeEndOrderActivity extends Activity implements
		ShakeDetector.Listener {

	private SensorManager sensorManager;
	private ShakeDetector sd;
	private Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		InkeeUtils.moverioScreenCustomization(this,
				SensorControl.SENSOR_MODE_CONTROLLER, getWindow(), 15, false);

		setContentView(R.layout.activity_inkee_end_order);

		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		sd = new ShakeDetector(this);

	}

	@Override
	protected void onResume() {
		super.onResume();

		handler.postDelayed(new Runnable() {
			@Override
			public void run() {

				sd.start(sensorManager);
				Toast.makeText(getApplicationContext(), "shake detector pronto all'uso",
						Toast.LENGTH_SHORT).show();

			}
		}, 5000);

		InkeeUtils.setActivityVisible(InkeeWaitOrdersActivity.class
				.getSimpleName());

	}

	public void restart(View v) {
		Intent intent = new Intent(this, InkeeLoginActivity.class);
		startActivity(intent);
	}

	/**
	 * When the user shakes the device, restart the demo.
	 */
	@Override
	public void hearShake() {
		Toast.makeText(this, "Riavvio demo in corso...", Toast.LENGTH_SHORT)
				.show();

		Intent intent = new Intent(this, InkeeLoginActivity.class);
		startActivity(intent);

	}
}
