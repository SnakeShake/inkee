package it.inkee.ar.activity.wait;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import it.inkee.ar.R;
import it.inkee.ar.activity.order.InkeeChooseOrder;
import it.inkee.ar.utils.InkeeUtils;
import jp.epson.moverio.bt200.SensorControl;

/**
 * This class is used to wait new orders for the logged user.
 * 
 * @author mutti
 * 
 */
public class InkeeWaitOrdersActivity extends Activity {

	private static final String TAG = InkeeWaitOrdersActivity.class
			.getSimpleName();

	Handler handler = new Handler();
	private boolean appInFront = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		InkeeUtils.moverioScreenCustomization(this,
				SensorControl.SENSOR_MODE_HEADSET, getWindow(), 10, false);

		setContentView(R.layout.activity_inkee_wait_orders);

		getWindow().getDecorView().setBackgroundColor(Color.BLACK);

	}

	/**
	 * DEMO PURPOSE. This method registers a handler and after 15s a new order
	 * is assigned to the user. Both visual and vocal message are used to notify
	 * the user. Furthermore, the activity and registers an observer
	 * (mMessageReceiver) to receive Intents with actions named
	 * {@link InkeeUtils.INKEE_ACTION_ORDER}.
	 */
	@Override
	protected void onResume() {
		super.onResume();

		appInFront = true;

		LocalBroadcastManager.getInstance(this).registerReceiver(
				mMessageReceiver,
				new IntentFilter(InkeeUtils.INKEE_ACTION_ORDER));

		Log.d(TAG, "LocalBroadcastManager for the action "
				+ InkeeUtils.INKEE_ACTION_ORDER + " succesfully registered");

		// show new order only if the user has accepted one
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {

				if (appInFront) {
					TextView tvNewOrder = (TextView) findViewById(R.id.textNewOrder);

					tvNewOrder.setTextColor(Color.RED);

					Animation anim = new AlphaAnimation(0.0f, 1.0f);
					anim.setDuration(1000); // You can manage the blinking time
											// with
											// this parameter
					anim.setStartOffset(20);
					anim.setRepeatMode(Animation.REVERSE);
					anim.setRepeatCount(Animation.INFINITE);
					tvNewOrder.startAnimation(anim);

					Intent intent = new Intent(InkeeUtils.INKEE_ACTION_SPEAK);
					intent.putExtra("message", "Nuovo ordine da evadere");
					LocalBroadcastManager.getInstance(getApplicationContext())
							.sendBroadcast(intent);
				}
			}
		}, 15000);

		InkeeUtils.setActivityVisible(InkeeWaitOrdersActivity.class
				.getSimpleName());
	}

	/**
	 * When pause the app, unregister the observer.
	 */
	@Override
	protected void onPause() {
		super.onPause();

		appInFront = false;

		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				mMessageReceiver);

		Log.d(TAG, "LocalBroadcast unregistered");

	}

	/**
	 * The handler for received Intents. This will be called whenever an Intent
	 * with an action named "INKEE_ACTION_ORDER" is broadcasted.
	 */
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (InkeeUtils.INKEE_ACTION_ORDER.equals(intent.getAction())) {
				startOrdersList();
				Log.d(TAG, "A new order is assigned to the user");
			}
		}
	};

	/**
	 * This method is used to start the InkeeChooseOrder activity. The
	 * InkeeChooseOrder activity can be used to accept or reject the order.
	 */
	public void startOrdersList() {
		Intent intent = new Intent(getApplicationContext(),
				InkeeChooseOrder.class);
		startActivity(intent);

		Log.d(TAG, "start InkeeChooseOrder activity");

	}

}
