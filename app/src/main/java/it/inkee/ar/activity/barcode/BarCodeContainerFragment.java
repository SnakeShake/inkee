package it.inkee.ar.activity.barcode;

import it.inkee.ar.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BarCodeContainerFragment extends Fragment {
	public BarCodeContainerFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.barcode_fragment_layout, container,
				false);
	}
}