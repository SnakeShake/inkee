package it.inkee.ar.activity.order;

import java.io.Serializable;

/**
 * 
 * Order class is used to represent an order.
 * 
 * @author mutti
 * 
 */
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	private String orderNumber;
	private String client;
	private String ubication;
	private String codArticle;
	private String description;
	private String qta;

	private boolean isAccepted = false;

	public Order(String orderNumber, String client, String ubication,
			String codArticle, String description, String qta,
			boolean isAccepted) {

		this.orderNumber = orderNumber;
		this.client = client;
		this.ubication = ubication;
		this.codArticle = codArticle;
		this.description = description;
		this.qta = qta;

		this.isAccepted = isAccepted;

	}

	public String getUbication() {
		return ubication;
	}

	public void setUbication(String ubication) {
		this.ubication = ubication;
	}

	public String getCodArticle() {
		return codArticle;
	}

	public void setCodArticle(String codArticle) {
		this.codArticle = codArticle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQta() {
		return qta;
	}

	public void setQta(String qta) {
		this.qta = qta;
	}

	public boolean getAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setNumProdotto(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

}
