package it.inkee.ar.activity.order;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.JsonReader;

/**
 * LoadOrder is a utility class used to load an order from a json file. The json
 * file is stored locally.
 * 
 * @author mutti
 * 
 */
public class LoadOrder {

	private List<Order> orders;

	public LoadOrder() {

	}

	/**
	 * This method allows to retrieve the order inside the json file.
	 * 
	 * @param context
	 *            Application context
	 * @return true if the file is read correctly, false otherwise.
	 */
	public boolean load(Context context) {

		InputStream is;
		try {
			is = context.getAssets().open("order/demo.json");

			orders = readOrders(is);

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @return a list of orders read from the json file.
	 */
	public List<Order> getOrders() {
		return orders;
	}

	public List<Order> readOrders(InputStream in) throws IOException {
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		try {
			return readOrdersInternal(reader);
		} finally {
			reader.close();
		}
	}

	public List<Order> readOrdersInternal(JsonReader reader) throws IOException {
		List<Order> o = new ArrayList<Order>();

		reader.beginArray();
		while (reader.hasNext()) {
			o.add(readOrder(reader));
		}
		reader.endArray();
		return o;
	}

	/**
	 * 
	 * This method allows to parse the json file.
	 * 
	 * @param reader
	 * @return a new order read from the file.
	 * @throws IOException
	 */
	public Order readOrder(JsonReader reader) throws IOException {
		String orderNumber = "";
		String client = "";
		String ubication = "";
		String codArt = "";
		String descr = "";
		int qta = -1;

		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("orderNumber")) {
				orderNumber = reader.nextString();
			} else if (name.equals("client")) {
				client = reader.nextString();
			} else if (name.equals("ubication")) {
				ubication = reader.nextString();
			} else if (name.equals("codArticle")) {
				codArt = reader.nextString();
			} else if (name.equals("description")) {
				descr = reader.nextString();
			} else if (name.equals("qta")) {
				qta = reader.nextInt();
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();
		return new Order(orderNumber, client, ubication, codArt, descr,
				Integer.toString(qta), false);
	}
}
