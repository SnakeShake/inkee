package it.inkee.ar.activity.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.artoolkit.ar.base.ARActivity;
import org.artoolkit.ar.base.rendering.ARRenderer;

import java.util.Calendar;
import java.util.Locale;

import it.inkee.ar.R;
import it.inkee.ar.activity.wait.InkeeWaitOrdersActivity;
import it.inkee.ar.application.InkeeApplication;
import it.inkee.ar.renderer.InkeeRendererOther;
import it.inkee.ar.utils.InkeeUtils;
import jp.epson.moverio.bt200.SensorControl;

/**
 * InkeeLoginActivity manages the login phase. A user can focus her badge and,
 * if she is a valid user, login to the app.
 * 
 * @author mutti
 */

public class InkeeLoginActivity extends ARActivity {

	private static final String TAG = InkeeLoginActivity.class.getSimpleName();

	Handler handler = new Handler();

	private boolean speak = false;

	/**
	 * Layout object used to show the camera preview and the opengl objects.
	 */
	private FrameLayout cameraLayout;
	private FrameLayout glviewLayout;
	private GLSurfaceView gl2View;
	private InkeeRendererOther inkeeRendererOther;

	/**
	 * This method allows to create the FragmentActivity, set the background to
	 * {@link Color.BLACK}.
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		InkeeUtils.moverioScreenCustomization(this,
				SensorControl.SENSOR_MODE_HEADSET, getWindow(), 10, false);

		setContentView(R.layout.activity_inkee_login);

		getWindow().getDecorView().setBackgroundColor(Color.BLACK);

	}

	/**
	 * When resume the app, register an observer (mMessageReceiver) to receive
	 * Intents with actions named {@link InkeeUtils.INKEE_ACTION_LOGIN}.
	 */
	@Override
	public void onResume() {
		super.onResume();

		speak = false;

		LocalBroadcastManager.getInstance(this).registerReceiver(
				mMessageReceiver,
				new IntentFilter(InkeeUtils.INKEE_ACTION_LOGIN));

		Log.d(TAG, "LocalBroadcast for the action "
				+ InkeeUtils.INKEE_ACTION_LOGIN + " succesfully registered");

		InkeeUtils.setActivityVisible(InkeeLoginActivity.class.getSimpleName());
	}

	/**
	 * When pause the app, unregister the observer.
	 */
	@Override
	protected void onPause() {
		super.onPause();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				mMessageReceiver);
	}

	@Override
	protected ARRenderer supplyRenderer() {
		Log.d(TAG, "Create Renderer object");

		inkeeRendererOther = new InkeeRendererOther(getApplicationContext(),
				((InkeeApplication) getApplication()).getOtherMarkers());

		return inkeeRendererOther;
	}

	@Override
	protected FrameLayout supplyCameraLayout() {
		cameraLayout = (FrameLayout) this.findViewById(R.id.cameraLayoutLogin);
		if( ((InkeeApplication) getApplication()).isInDemoMode() ){
			cameraLayout.setLayoutParams(new ViewGroup.LayoutParams(-1,-1));
		}
		Log.d(TAG, "Camera layout supplied");
		return cameraLayout;
	}

	@Override
	protected FrameLayout supplyGLViewLayout() {
		glviewLayout = (FrameLayout) this.findViewById(R.id.glviewLayoutLogin);
		Log.d(TAG, "GLView layout supplied");
		return glviewLayout;
	}

	@Override
	protected GLSurfaceView supplyGLView() {
		return gl2View;
	}

	/**
	 * The handler for received Intents. This will be called whenever an Intent
	 * with an action named "INKEE_ACTION_LOGIN" is broadcasted.
	 */
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (InkeeUtils.INKEE_ACTION_LOGIN.equals(intent.getAction())) {
				String user = intent.getExtras().getString("user") != null ? intent
						.getExtras().getString("user") : "invalid";
				Log.d(TAG, "user" + user + " wants to login to the app");
				login(user);
			}
		}
	};

	/**
	 * This method is used to start the InkeeWaitOrdersActivity activity after
	 * that a user is successfully logged in.
	 * 
	 * @param authUser
	 *            authenticated user
	 */
	public void startWaitOrdersActivity(String authUser) {

		welcomeMessage(authUser);

		startWaitActivity(authUser);

	}

	private void startWaitActivity(String authUser) {
		Intent intent = new Intent(this, InkeeWaitOrdersActivity.class);
		intent.putExtra("user", authUser);
		startActivity(intent);
		Log.d(TAG, "Start InkeeWaitOrdersActivity");

	}

	/**
	 * This method checks if the user is a valid user for the app.
	 * 
	 * @param user
	 *            the user info coming from the badge
	 */
	protected void login(String user) {
		for (String u : InkeeUtils.authUsers) {
			if (user.equals(u)) {
				Log.i(TAG, "user authenticated correctly");
				startWaitOrdersActivity(user);
				break;
			}
		}

	}

	// @Override
	// protected void onDestroy() {
	// super.onDestroy();
	//
	// finish();
	// }

	/**
	 * This methods allows to welcome the user after the login. An sms will be
	 * sent to the class used to manage the speak process.
	 * 
	 * @param authUser
	 *            authenticated user
	 */
	private void welcomeMessage(String authUser) {

		Calendar sCalendar = Calendar.getInstance();
		String dayLongName = sCalendar.getDisplayName(Calendar.DAY_OF_WEEK,
				Calendar.LONG, Locale.ITALY);

		int dayNumberLongName = sCalendar.get(Calendar.DAY_OF_MONTH);

		String monthLongName = sCalendar.getDisplayName(Calendar.MONTH,
				Calendar.LONG, Locale.ITALY);

		// String sms = "Buongiorno " + authUser + "";
		// sms += " oggi è " + dayLongName + " ";
		//
		// sms += dayNumberLongName + " di " + monthLongName;

		if (!speak) {
			String sms = "Benvenuto nella demo di Inki"; // trick for Inkee

			Intent intent = new Intent(InkeeUtils.INKEE_ACTION_SPEAK);
			intent.putExtra("message", sms);
			LocalBroadcastManager.getInstance(getApplicationContext())
					.sendBroadcast(intent);

			speak = true;
		}

	}

}
